# External lib build

## Requirements:

1. Docker

2. `make` 

3. Huge amount of RAM and free disk space

## Building:

1.  Clone monero repo: `git clone https://github.com/m2049r/monero`

2.  Go to monero folder: `cd monero` & change repo to latest branch (example: `git checkout release-v0.17.3.2-monerujo`)
        
        - Check https://github.com/m2049r/monero for latest branch

3.  Update submodules: `git submodule update --init --force` & exit to home folder: `cd`

4.  Clone shruum repo: `git clone https://code.samourai.io/r4v3r23/shruum.git`

5.  Go to shruum's external-libs folder: `cd shruum/external-libs`

6.  Create symbol link to monero folder: `ln -s ~/monero ~/shruum/external-libs/monero`

7.  Start Docker and then run: `make` in external-libs folder.
       
8. Open shruum folder as project in Android Studio > Build > Generate Signed Bundle / APK

9. Select APK, choose existing/create new signing key & select build variant (example: prodMainnetRelease)

10. Install APK & fuck the state
